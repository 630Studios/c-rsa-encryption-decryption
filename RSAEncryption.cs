/*
 * Created By: Jason Penick 
 * Date: 1/17/2016
 * Description:
 * This is a simple wrapper class for the RSACryptoServiceProvider class making it easy to 
 * get public/private key pairs as well as several functions to make encrypting and decrypting
 * data easier.
 *
 * Do what you want with it, its not like this is some mad stroke of genius.
 */
using System;
using System.Security.Cryptography;
using System.Text;
using System.Collections.Generic;


public class RSAEncryption
{

	public static Dictionary<string,string> getKeys()
	{
		Dictionary<string, string> keys = new Dictionary<string, string>();
		RSACryptoServiceProvider csp = new RSACryptoServiceProvider(2048);
		
		keys.Add("public", csp.ToXmlString(false));
		keys.Add("private", csp.ToXmlString(true));

		csp.PersistKeyInCsp = false;

		return keys;
	}




	public static byte[] decrypt(byte[] message, RSAParameters privateKey)
	{
		RSACryptoServiceProvider csp = new RSACryptoServiceProvider();
		csp.ImportParameters(privateKey);
		return csp.Decrypt(message, false);

	}


	public static string decryptString(byte[] message, RSAParameters privateKey)
	{
		RSACryptoServiceProvider csp = new RSACryptoServiceProvider();
		csp.ImportParameters(privateKey);
		return ASCIIEncoding.ASCII.GetString(csp.Decrypt(message, false));

	}

	public static string decryptString(string message, RSAParameters privateKey)
	{
		RSACryptoServiceProvider csp = new RSACryptoServiceProvider();
		csp.ImportParameters(privateKey);
		return Encoding.UTF8.GetString(csp.Decrypt(Convert.FromBase64String(message), false));

	}



	public static byte[] decrypt(byte[] message, string privateKeyXML)
	{
		RSACryptoServiceProvider csp = new RSACryptoServiceProvider();
		csp.FromXmlString(privateKeyXML);
		return csp.Decrypt(message, false);

	}

	public static string decryptToString(byte[] message, string privateKeyXML)
	{
		RSACryptoServiceProvider csp = new RSACryptoServiceProvider();
		csp.FromXmlString(privateKeyXML);
		return ASCIIEncoding.ASCII.GetString(csp.Decrypt(message, false));

	}

	public static string decryptToString(string message, string privateKeyXML)
	{
		RSACryptoServiceProvider csp = new RSACryptoServiceProvider();
		csp.FromXmlString(privateKeyXML);
		return Encoding.UTF8.GetString(csp.Decrypt(Convert.FromBase64String(message), false));

	}



	#region Encrypt functions

	public static byte[] encrypt(byte[] message, RSAParameters publicKey)
	{

		RSACryptoServiceProvider csp = new RSACryptoServiceProvider();
		csp.ImportParameters(publicKey);
		return csp.Encrypt(message, false);

	}

	public static byte[] encrypt(string message, RSAParameters publicKey)
	{

		RSACryptoServiceProvider csp = new RSACryptoServiceProvider();
		csp.ImportParameters(publicKey);
		return csp.Encrypt(Encoding.UTF8.GetBytes(message), false);

	}

	public static string encryptToString(string message, RSAParameters publicKey)
	{

		RSACryptoServiceProvider csp = new RSACryptoServiceProvider();
		csp.ImportParameters(publicKey);
		return Convert.ToBase64String(csp.Encrypt(Encoding.UTF8.GetBytes(message), false));

	}

	public static byte[] encrypt(byte[] message, string publicKeyXML)
	{

		RSACryptoServiceProvider csp = new RSACryptoServiceProvider();
		csp.FromXmlString(publicKeyXML);
		return csp.Encrypt(message, false);

	}

	public static byte[] encrypt(string message, string publicKeyXML)
	{

		RSACryptoServiceProvider csp = new RSACryptoServiceProvider();
		csp.FromXmlString(publicKeyXML);
		return csp.Encrypt(Encoding.UTF8.GetBytes(message), false);

	}

	public static string encryptToString(string message, string publicKeyXML)
	{

		RSACryptoServiceProvider csp = new RSACryptoServiceProvider();
		csp.FromXmlString(publicKeyXML);
		return Convert.ToBase64String(csp.Encrypt(Encoding.UTF8.GetBytes(message), false));

	}

	#endregion
}
