# CSharp RSA Encryption/Decription Class#

This is a basic wrapper for the .net RSACryptoServiceProvider class that provides easy functionality for getting public/private key pairs as well as several different methods for encryption and decryption using both the RSAParameters class and XML versions of the public and private keys.


# How to Use #  
  
To use the the class simply download the RSAEncryption.cs file and put it somewhere in your project.
This will give you access to the RSAEncryption class.  
  
# Functions #

**For Getting Keys**  
```csharp
public static Dictionary<string,string> getKeys() 
```
  
**Encryption**  
```csharp
public static byte[] encrypt(byte[] message, RSAParameters publicKey)  
public static byte[] encrypt(string message, RSAParameters publicKey)  
public static byte[] encrypt(byte[] message, string publicKeyXML)  
public static byte[] encrypt(string message, string publicKeyXML)  
public static string encryptToString(string message, string publicKeyXML)  
public static string encryptToString(string message, RSAParameters publicKey)  
```
  

**Decryption** 
```csharp
public static byte[] decrypt(byte[] message, RSAParameters privateKey)    
public static byte[] decrypt(byte[] message, string privateKeyXML)  
public static string decryptToString(byte[] message, string privateKeyXML)  
public static string decryptToString(string message, string privateKeyXML)  
public static string decryptString(byte[] message, RSAParameters privateKey)  
public static string decryptString(string message, RSAParameters privateKey)
```